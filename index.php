<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    print('Спасибо, результаты сохранены.');
  }
  include('form.php');
  exit();
}

$errors = FALSE;
if (empty($_POST['name'])) {
  print('Введите имя<br/>');
  $errors = TRUE;
}

if (empty($_POST['email'])) {
  print('Введите почту<br/>');
  $errors = TRUE;
}

if (empty($_POST['date'])) {
  print('Введите дату рождения<br/>');
  $errors = TRUE;
}

if (empty($_POST['gender'])) {
  print('Выбирите ваш пол<br/>');
  $errors = TRUE;
}

if (empty($_POST['limbs'])) {
  print('Заполните поле конечности<br/>');
  $errors = TRUE;
}

if (empty($_POST['accept'])) {
  print('Вы должны согласиться с условиями<br/>');
  $errors = TRUE;
}

if ($errors) {
  exit();
}

try {
  $user = 'u24309';
  $pass = '6956108';
  $db = new PDO('mysql:host=localhost;dbname=u24309', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
  $stmt = $db->prepare("INSERT INTO application SET name = ?, email = ?, date = ?, gender = ?, limbs = ?, biography = ?");
  $stmt->execute([$_POST['name'], $_POST['email'], $_POST['date'], $_POST['gender'], $_POST['limbs'], $_POST['biography']]);
  $last = $db->lastInsertId();
  $sp = $_POST['superpowers'];
  if (!empty($sp)) {
    foreach ($sp as $ability) {
      $stmt = $db->prepare("INSERT INTO superpowers (user_id, ability) VALUES (:user_id, :ability)");
      $stmt -> execute(array(
        'user_id' => $last,
        'ability' => $ability
      ));
    }
  }

} catch (PDOException $e) {
  print('Error : ' . $e->getMessage());
  exit();
}

header('Location: ?save=1');
