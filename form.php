<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Форма</title>
</head>
<body>
<section class="form-section">
    <h2>Пожалуйста, заполните форму</h2>
    <form method="POST" class="form" id="form" action="index.php" accept-charset="UTF-8">
        <div class="name-email_container">
            <div class="name-container">
                <label for="name">Имя:</label>
                <input type="text" id="name" name="name" placeholder="Введите своё имя">
            </div>
            <div class="email-container">
                <label for="email">Email:</label>
                <input type="email" id="email" name="email" placeholder="Введите свой email">
            </div>
        </div>
        <label for="date">Дата рождения:</label>
        <input type="date" id="date" name="date">
        <div class="gender-container">
            <p>Пол:</p>
            <input type="radio" name="gender" value="male" id="gender-male">
            <label for="gender-male">Мужчина</label>
            <input type="radio" name="gender" value="female" id="gender-female">
            <label for="gender-female">Женщина</label>
        </div>
        <div class="limbs-container">
            <p>Количество конечностей:</p>
            <input type="radio" name="limbs" value="1" id="limbs-1">
            <label for="limbs-1">1</label>
            <input type="radio" name="limbs" value="2" id="limbs-2">
            <label for="limbs-2">2</label>
            <input type="radio" name="limbs" value="4" id="limbs-4">
            <label for="limbs-4">4</label>
            <input type="radio" name="limbs" value="6" id="limbs-6">
            <label for="limbs-6">6</label>
            <input type="radio" name="limbs" value="8" id="limbs-8">
            <label for="limbs-8">8</label>
        </div>
        <label for="superpowers">Сверхспособности:</label>
        <select name="superpowers" id="superpowers" multiple size="3">
            <option value="immortality">Бессмертие</option>
            <option value="walking throw walls">Прохождение сквозь стены</option>
            <option value="levitation">Левитация</option>
        </select>
        <label for="biography">Биография:</label>
        <textarea name="biography"
                  id="biography"
                  cols="30"
                  rows="10"
                  class="biography-input"
                  placeholder="Введите вашу биографию"></textarea>
        <div class="accept-container">
            <input type="checkbox" id="accept" name="accept">
            <label for="accept">C контрактом ознакомлен</label>
        </div>
        <input type="submit" value="Отправить" class="btn">
    </form>
</section>
</body>
</html>